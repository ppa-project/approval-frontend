/**
 * Copyright 2021 TNO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Version from './views/Version.vue';

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  base: process.env.NODE_ENV === 'production' ? '/approve' : '/',
  routes: [
    {
      path: '/',
      name: 'root',
      component: Home
    },
    {
      path: '/home',
      redirect: {
        name: 'root'
      }
    },
    {
      path: '/version',
      component: Version
    }
  ]
});
