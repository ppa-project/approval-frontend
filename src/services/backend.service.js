/**
 * Copyright 2021 TNO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import axios from 'axios';

const API_URL = process.env.VUE_APP_API || '/api';

class BackendService {
  getVersion() {
    return axios.get(API_URL + '/version');
  }

  getPendingApprovals() {
    return axios.get(API_URL + '/approvals');
  }

  getPendingApproval(queryID) {
    return axios.get(API_URL + '/approvals/' + queryID);
  }

  approveQuery(queryID) {
    return axios.post(API_URL + '/approvals/' + queryID, { approve: true });
  }
  rejectQuery(queryID) {
    return axios.post(API_URL + '/approvals/' + queryID, { approve: false });
  }
}

export default new BackendService();
